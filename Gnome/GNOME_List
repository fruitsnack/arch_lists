#----------------------------------------------------------------------------
# "THE COFFEE-WARE LICENSE" (based on the beer-ware license revision 42):
# Fruitsnack wrote this file. As long as you retain this notice you
# can do whatever you want with this stuff. If we meet some day, and you think
# this stuff is worth it, you can buy me a coffee in return.
#----------------------------------------------------------------------------

# Desktop components
gdm
gnome-backgrounds
gnome-color-manager
gnome-control-center
gnome-disk-utility
gnome-keyring
gnome-logs
gnome-menus
gnome-remote-desktop
gnome-screenshot
gnome-session
gnome-settings-daemon
gnome-shell
gnome-shell-extensions
gnome-system-monitor
gnome-themes-extra
gnome-user-docs
gnome-user-share
gnome-video-effects
grilo-plugins
gvfs
gvfs-gphoto2
gvfs-mtp
gvfs-nfs
gvfs-smb
gvfs-goa
gvfs-afc
mutter
orca
rygel
sushi
tracker
tracker-miners
tracker3
tracker3-miners
vino
xdg-user-dirs-gtk
yelp
gnome-tweaks

# Extensions
gnome-shell-extension-appindicator

# Files
file-roller
baobab
nautilus
nautilus-sendto
nautilus-share
python-nautilus
seahorse-nautilus
phodav

# PIM
gnome-contacts
gnome-todo
gnome-calendar

# Acessories
gnome-clocks
gnome-calculator
gnome-characters
gnome-font-viewer
gnome-logs
gnome-maps
gnome-weather
gnome-dictionary
gimagereader-gtk
tilix
gedit
gedit-plugins
gnome-usage

# Security
gnome-passwordsafe
seahorse
gnupg
openssl
tor
torsocks
torbrowser-launcher
nyx

# Base System
base
base-devel
cryptsetup
device-mapper
dhcpcd
diffutils
grub
lvm2
man-db
netctl
perl
sysfsutils
usbutils

# System
xorg-server
xorg-xrandr
bluez
bluez-utils
cups
cups-pdf
gutenprint
jack2
pulseaudio
pulseaudio-alsa
pulseaudio-jack
pulseaudio-bluetooth
xf86-input-wacom
wine-staging
wine-gecko
wine-mono
winetricks
linux-firmware
linux-zen
linux-zen-headers
realtime-privileges
networkmanager
samba
openssh
ufw
gufw
acpid
acpi
man-pages
licenses
firejail
efibootmgr
amd-ucode
dnscrypt-proxy
privoxy
vulkan-tools
colord
cronie

# Virtualization
qemu
libvirt
gnome-boxes

# Office
libreoffice-fresh
evince
foliate
simple-scan

# Internet
dino
polari
element-desktop
syncthing
fragments
gfeeds
gnome-podcasts
geary
gnome-connections
cawbird
warpinator

# Graphics
gimp
blender
krita
kseexpr
krita-plugin-gmic
gmic
inkscape
gthumb
gcolor3
drawing
curtail
variety

# Multimedia
blanket
gnome-sound-recorder
peek
cheese
totem
lollypop
obs-studio
pitivi
ardour
harvid
calf
guitarix
helm-synth
artyfx
fabla
cadence
geonkick
surge

# DE Dev Tools
gnome-builder
glade
sysprof
dconf-editor
gitg
d-feet
devhelp
gnome-devel-docs
gnome-code-assistance
godot
gnome-firmware

# Development
flashrom
doxygen
graphviz
ctags
lldb
gdb
radare2
valgrind
git
subversion
mercurial
jdk8-openjdk
gcc
avr-gcc
avr-gdb
avr-libc
clang
go
rust
python
python-pip
cython
lua
luarocks
lua-sec
fasm
cmake
extra-cmake-modules
scons
meson
ninja
android-tools
avrdude
simavr
pkgconf
python-pylint
haxe
love
npm
stylelint
stylelint-config-standard
jedi-language-server
eslint

# Libs
glibc
gst-libav
gstreamer
gst-plugins-good
gst-plugins-bad
gst-plugins-ugly
mpg123
lib32-gtk2
lib32-gtk3
lib32-openal
lib32-mpg123
lib32-libpulse
lib32-jack2
lib32-gstreamer
lib32-gst-plugins-good
lib32-gst-plugins-base
tesseract
tesseract-data-eng
tesseract-data-rus
lib32-mesa
#vulkan-radeon
#lib32-vulkan-radeon
vulkan-icd-loader
lib32-vulkan-icd-loader
vkd3d
lib32-vkd3d
vulkan-mesa-layers
lib32-vulkan-mesa-layers
taglib
lib32-taglib
libappindicator-gtk2
libappindicator-gtk3
lib32-libappindicator-gtk2
lib32-libappindicator-gtk3
steam-native-runtime
amdvlk
lib32-amdvlk

# Utilities
nano
which
neofetch
parted
testdisk
jq
wget
mpd
less
youtube-dl
net-tools
inetutils
ffmpeg
dmidecode
festival
festival-us
speech-dispatcher
whois
fish

# Thumbnails
xcftools
ffmpegthumbnailer

# Fonts
noto-fonts
noto-fonts-extra
noto-fonts-emoji
noto-fonts-cjk
cantarell-fonts
adobe-source-code-pro-fonts
inter-font
ttf-sarasa-gothic
ttf-roboto
ttf-roboto-mono
ttf-jetbrains-mono
gnu-free-fonts
ttf-ibm-plex

# GTK Themes
materia-gtk-theme

# Icon Themes
papirus-icon-theme

# Cursor Themes
capitaine-cursors

# KDE/Kvantum Themes
kvantum-theme-materia
kvantum-qt5
materia-kde
qt5ct

# Filesystem Support
e2fsprogs
xfsprogs
jfsutils
reiserfsprogs
ntfs-3g
dosfstools
f2fs-tools
exfat-utils
nilfs-utils
udftools
encfs
cryfs
mtools
fatresize
gpart
squashfs-tools
sshfs
go-ipfs
gocryptfs

# Archive support
p7zip
tar
bzip2
gzip
xz
arj
unrar
lzip
zip
unzip
lrzip
lzop

# Pentest
wireshark-qt
nmap
aircrack-ng
reaver
hashcat

# Games
minetest
xonotic
openmw
dosbox
retroarch
retroarch-assets-xmb
retroarch-assets-ozone
retroarch-assets-glui
libretro-core-info
libretro-overlays
libretro-shaders-slang
libretro-beetle-pce-fast
libretro-beetle-psx-hw
libretro-beetle-supergrafx
libretro-blastem
libretro-bsnes2014
libretro-citra
libretro-desmume
libretro-dolphin
libretro-flycast
libretro-gambatte
libretro-kronos
libretro-melonds
libretro-mesen-s
libretro-mgba
libretro-mupen64plus-next
libretro-nestopia
libretro-parallel-n64
libretro-play
libretro-ppsspp
libretro-retrodream
libretro-sameboy
libretro-scummvm
libretro-yabause
wesnoth
lutris
gamemode
lib32-gamemode
piper
