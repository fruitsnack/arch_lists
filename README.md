# Arch Linux Package Lists
This is my personal collection of lists of packages to be used with my
[script](https://codeberg.org/fruitsnack/pac_manager) for declarative package
management on arch linux and derivatives.

"Platform" dir contains package lists specific to various machines.

Note: only XFCE and KDE lists are currently up to date.

## Todo
- Add pinephone phosh and plasma lists
- Add cinnamon list
- Add EXWM list

## License:
Lists are licensed under coffeeware (fork of beerware) license.

